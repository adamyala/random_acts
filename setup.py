"""
Standard Setup script
"""
import datetime

from setuptools import setup, find_packages

setup(
    name="RandomActsApp",
    version="0.%s" % datetime.datetime.now().strftime("%s"),
    packages=find_packages(),
    author="Adam Yala and William Clemens",

    install_requires=[
        'Flask-Cache>=0.13.1',
        'Flask-SQLAlchemy>=2.0',
        'Flask-Script>=2.0.5',
        'Flask>=0.10.1',
        #'redis>=2.10.3', # Needed if using redis as a cache engine
        'psycopg2>=2.6.1',
        'sendgrid>=1.4.0',
        'twilio>=4.4.0',
        'braintree>=3.16.0'
    ],

    package_data={
        'randomacts': [
            'static/*',
            'templates/*',
        ],
    },

    entry_points={
        'console_scripts': [
            'randomacts=randomacts.command:main_cli',
        ]
    },
)
