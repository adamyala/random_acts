import braintree
import httplib
import sendgrid
from flask import Response, jsonify, request, url_for, redirect, json
from sqlalchemy.sql.expression import func


from .. import app, cache, db
from ..models import Request, Donation, Thankyou

braintree.Configuration.configure(
        braintree.Environment.Sandbox,
        'gt9mh24vz448bsz7',
        'jkrmh8sd9v82hbgz',
        '2295394e919222e881b4ef60ae7f8ccd'
        )

sg = sendgrid.SendGridClient('wesclemens', '%dx5Do[erq{$V%%j<(j^MhP\N~$d2UK6jA,VSr^8', raise_errors=True)

def api_result(status_code, reason_code=0, reason=''):
    try:
        response = jsonify(
                status_code=status_code,
                message = httplib.responses[status_code],
                reason_code = reason_code,
                reason=reason,
                )
        response.status_code = status_code
    except IndexError:
        response = jsonify(
                status_code=500,
                message=httplib.responses[500],
                reason_code=999,
                reason="API tried to return invalid status code",
                )
        response.status_code = 500
    return response

@app.route('/api/list', methods=['GET'])
def get_list():
    def generate():
        for req in Request.query.order_by(func.random()).all():
            yield {
                'id':req.id,
                'first_name':req.first_name,
                'needs':req.needs,
                'amount':req.amount,
                'city':req.city,
                'story':req.story,
            }

    return json.dumps(list(generate()))

@app.route('/api/thankyous', methods=['GET'])
def get_thankyous():
    def generate():
        for req in Thankyou.query.order_by(func.random()).all():
            yield {
                'name':req.name,
                'thankyou':req.thankyou,
                'created_on':req.created_on,
            }

    return json.dumps(list(generate()))

@app.route('/api/request/<int:request_id>', methods=['GET'])
def get_request(request_id):
    req = Request.query.get(request_id)
    return jsonify(
            id=req.id,
            first_name=req.first_name,
            needs=req.needs,
            amount=req.amount,
            city=req.city,
            story=req.story,
            )

@app.route('/api/request', methods=['POST'])
def get_requests():
    post_data = request.get_json()
    req = Request(
        needs=post_data.get('needs',''),
        amount=post_data['amount'],
        email=post_data.get('email',''),
        phone=post_data.get('phone',''),
        first_name=post_data['first_name'],
        last_name=post_data.get('last_name',''),
        address_1=post_data.get('address_1',''),
        address_2=post_data.get('address_2',''),
        city=post_data['city'],
        zip_code=post_data.get('zip_code',''),
        story=post_data['story'],
        delievery_notes=post_data.get('notes',''),
    )

    db.session.add(req)
    db.session.commit()

    return api_result(status_code=200)

@app.route("/api/payment/client_token", methods=["GET"])
def client_token():
    return braintree.ClientToken.generate()

@app.route("/api/payment/submit", methods=['POST'])
def create_purchase():
    donation = Donation(
            name=request.form['name'],
            email=request.form['email'],
            amount=request.form['amount'],
            )

    db.session.add(donation)

    result = braintree.Transaction.sale({
        "amount": request.form['amount'], #"amount": "10.00",
        "payment_method_nonce": request.form["payment_method_nonce"]
        })

    db.session.commit()

    message = sendgrid.Mail(
            to=request.form['email'],
            subject='Thanks for your donation!',
            html='Thanks for your donation of {0}'.format(request.form['amount']),
            text='Thanks for your donation of {0}'.format(request.form['amount']),
            from_email='no-reply@randomacts.link')

    status, msg = sg.send(message)
    return redirect(url_for('home', _anchor='/confirmation'))

