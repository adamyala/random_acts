from flask import render_template
from time import gmtime, strftime

from .. import app, cache

@app.route("/", methods=['GET'])
@cache.cached(timeout=20)
def home():
    return render_template('index.html')

