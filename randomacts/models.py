"""
This is the logic for deailing with Foo Database entiteies
"""

from . import db

class Request(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    needs = db.Column(db.String(80), nullable=False)
    amount = db.Column(db.String(10), nullable=False)
    email = db.Column(db.String(120), nullable=False)
    phone = db.Column(db.String(25), nullable=False)
    first_name = db.Column(db.String(25), nullable=False)
    last_name = db.Column(db.String(25), nullable=False)
    address_1 = db.Column(db.String(80), nullable=False)
    address_2 = db.Column(db.String(80))
    city = db.Column(db.String(30), nullable=False)
    zip_code = db.Column(db.String(9), nullable=False)
    story = db.Column(db.Text, nullable=False)
    delievery_notes = db.Column(db.Text)
    created_on = db.Column(db.DateTime, server_default=db.func.now())
    updated_on = db.Column(db.DateTime, server_default=db.func.now(), onupdate=db.func.now())

class Donation(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), nullable=False)
    email = db.Column(db.String(120), nullable=False)
    amount = db.Column(db.Numeric(6,2), nullable=False)
    created_on = db.Column(db.DateTime, server_default=db.func.now())
    updated_on = db.Column(db.DateTime, server_default=db.func.now(), onupdate=db.func.now())

class Thankyou(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), nullable=False)
    email = db.Column(db.String(120), nullable=False)
    thankyou = db.Column(db.Text, nullable=False)
    created_on = db.Column(db.DateTime, server_default=db.func.now())
    updated_on = db.Column(db.DateTime, server_default=db.func.now(), onupdate=db.func.now())