(function(angular) {
angular.module('randomActs', ['ngRoute', 'ngResource'])
  .factory("getRequest", function($resource) {
    return $resource("/api/request/:id");
  })
  .factory("getRequests", function($resource) {
    return $resource("/api/list");
  })
  .factory("getThankyous", function($resource) {
    return $resource("/api/thankyous");
  })
  .factory("postRequest", function($resource) {
    return $resource("/api/request");
  })
  .config(function($routeProvider, $locationProvider, $resourceProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'static/requests.html',
        controller: 'requestsController',
        activeTab: 'acts',
      })
      .when('/request', {
        templateUrl: 'static/request.html',
        controller: 'requestController',
      })
      .when('/request_confirmation', {
        templateUrl: 'static/request_confirmation.html',
        controller: 'requestConfirmationController'
      })
      .when('/request_donation', {
        templateUrl: 'static/request_donation.html',
        controller: 'requestDonationController',
      })
      .when('/donation', {
        templateUrl: 'static/donation.html',
        controller: 'donationController',
        activeTab: 'donation',
      })
      .when('/confirmation', {
        templateUrl: 'static/confirmation.html',
        controller: 'requestsController',
      })
      .when('/thankyou', {
        templateUrl: 'static/thankyou.html',
        controller: 'thankyouController',
      })
  })
  .controller('navbarController', function navbarController($scope, $location) 
      {
        $scope.isActive = function (viewLocation) { 
          return viewLocation === $location.path();
        };
      }
  )
  .controller('requestsController', function($scope, $route, getRequests) {
    getRequests.query(function(data) {
      $scope.acts = data;
    });
  })
  .controller('thankyouController', function($scope, $route, getThankyous) {
    getThankyous.query(function(data) {
      $scope.thankyous = data;
    });
  })
  .controller('requestController', function($scope, $route, postRequest) {
    $scope.request = {};
    $scope.route = $route;
    $scope.submitRequest = function() {
      postRequest.save($scope.request);
    };
  })
  .controller('requestConfirmationController', function($scope, $route) {

  })
  .controller('donationController', function($scope, $route) {
    $scope.route = $route;
    braintree.setup(
      "eyJ2ZXJzaW9uIjoyLCJhdXRob3JpemF0aW9uRmluZ2VycHJpbnQiOiI3Mjg5MDhjMTk2MjI4MTE3N2MyM2QzYTQzMzhjMTI3MzkxNDM3N2VlMWFjMTk4Y2UxNDY2MWI5NjNjNDg5MGNifGNyZWF0ZWRfYXQ9MjAxNS0wNi0yOFQwMDozMjoyNS42NzM0OTUwODkrMDAwMFx1MDAyNm1lcmNoYW50X2lkPWd0OW1oMjR2ejQ0OGJzejdcdTAwMjZwdWJsaWNfa2V5PWprcm1oOHNkOXY4MmhiZ3oiLCJjb25maWdVcmwiOiJodHRwczovL2FwaS5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tOjQ0My9tZXJjaGFudHMvZ3Q5bWgyNHZ6NDQ4YnN6Ny9jbGllbnRfYXBpL3YxL2NvbmZpZ3VyYXRpb24iLCJjaGFsbGVuZ2VzIjpbXSwiZW52aXJvbm1lbnQiOiJzYW5kYm94IiwiY2xpZW50QXBpVXJsIjoiaHR0cHM6Ly9hcGkuc2FuZGJveC5icmFpbnRyZWVnYXRld2F5LmNvbTo0NDMvbWVyY2hhbnRzL2d0OW1oMjR2ejQ0OGJzejcvY2xpZW50X2FwaSIsImFzc2V0c1VybCI6Imh0dHBzOi8vYXNzZXRzLmJyYWludHJlZWdhdGV3YXkuY29tIiwiYXV0aFVybCI6Imh0dHBzOi8vYXV0aC52ZW5tby5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tIiwiYW5hbHl0aWNzIjp7InVybCI6Imh0dHBzOi8vY2xpZW50LWFuYWx5dGljcy5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tIn0sInRocmVlRFNlY3VyZUVuYWJsZWQiOnRydWUsInRocmVlRFNlY3VyZSI6eyJsb29rdXBVcmwiOiJodHRwczovL2FwaS5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tOjQ0My9tZXJjaGFudHMvZ3Q5bWgyNHZ6NDQ4YnN6Ny90aHJlZV9kX3NlY3VyZS9sb29rdXAifSwicGF5cGFsRW5hYmxlZCI6dHJ1ZSwicGF5cGFsIjp7ImRpc3BsYXlOYW1lIjoiUmFuZG9tQWN0cyIsImNsaWVudElkIjpudWxsLCJwcml2YWN5VXJsIjoiaHR0cDovL2V4YW1wbGUuY29tL3BwIiwidXNlckFncmVlbWVudFVybCI6Imh0dHA6Ly9leGFtcGxlLmNvbS90b3MiLCJiYXNlVXJsIjoiaHR0cHM6Ly9hc3NldHMuYnJhaW50cmVlZ2F0ZXdheS5jb20iLCJhc3NldHNVcmwiOiJodHRwczovL2NoZWNrb3V0LnBheXBhbC5jb20iLCJkaXJlY3RCYXNlVXJsIjpudWxsLCJhbGxvd0h0dHAiOnRydWUsImVudmlyb25tZW50Tm9OZXR3b3JrIjp0cnVlLCJlbnZpcm9ubWVudCI6Im9mZmxpbmUiLCJ1bnZldHRlZE1lcmNoYW50IjpmYWxzZSwiYnJhaW50cmVlQ2xpZW50SWQiOiJtYXN0ZXJjbGllbnQzIiwibWVyY2hhbnRBY2NvdW50SWQiOiJyYW5kb21hY3RzIiwiY3VycmVuY3lJc29Db2RlIjoiVVNEIn0sImNvaW5iYXNlRW5hYmxlZCI6ZmFsc2UsIm1lcmNoYW50SWQiOiJndDltaDI0dno0NDhic3o3IiwidmVubW8iOiJvZmYifQ==",
      "dropin", 
      {
        container: "payment-form",
      }
    );

  })
  .controller('requestDonationController', function($scope, $route, getRequest) {
    getRequest.get({ id: $route.current.params.id }, function(data) {
      $scope.act = data;
    });
    braintree.setup(
      "eyJ2ZXJzaW9uIjoyLCJhdXRob3JpemF0aW9uRmluZ2VycHJpbnQiOiI3Mjg5MDhjMTk2MjI4MTE3N2MyM2QzYTQzMzhjMTI3MzkxNDM3N2VlMWFjMTk4Y2UxNDY2MWI5NjNjNDg5MGNifGNyZWF0ZWRfYXQ9MjAxNS0wNi0yOFQwMDozMjoyNS42NzM0OTUwODkrMDAwMFx1MDAyNm1lcmNoYW50X2lkPWd0OW1oMjR2ejQ0OGJzejdcdTAwMjZwdWJsaWNfa2V5PWprcm1oOHNkOXY4MmhiZ3oiLCJjb25maWdVcmwiOiJodHRwczovL2FwaS5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tOjQ0My9tZXJjaGFudHMvZ3Q5bWgyNHZ6NDQ4YnN6Ny9jbGllbnRfYXBpL3YxL2NvbmZpZ3VyYXRpb24iLCJjaGFsbGVuZ2VzIjpbXSwiZW52aXJvbm1lbnQiOiJzYW5kYm94IiwiY2xpZW50QXBpVXJsIjoiaHR0cHM6Ly9hcGkuc2FuZGJveC5icmFpbnRyZWVnYXRld2F5LmNvbTo0NDMvbWVyY2hhbnRzL2d0OW1oMjR2ejQ0OGJzejcvY2xpZW50X2FwaSIsImFzc2V0c1VybCI6Imh0dHBzOi8vYXNzZXRzLmJyYWludHJlZWdhdGV3YXkuY29tIiwiYXV0aFVybCI6Imh0dHBzOi8vYXV0aC52ZW5tby5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tIiwiYW5hbHl0aWNzIjp7InVybCI6Imh0dHBzOi8vY2xpZW50LWFuYWx5dGljcy5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tIn0sInRocmVlRFNlY3VyZUVuYWJsZWQiOnRydWUsInRocmVlRFNlY3VyZSI6eyJsb29rdXBVcmwiOiJodHRwczovL2FwaS5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tOjQ0My9tZXJjaGFudHMvZ3Q5bWgyNHZ6NDQ4YnN6Ny90aHJlZV9kX3NlY3VyZS9sb29rdXAifSwicGF5cGFsRW5hYmxlZCI6dHJ1ZSwicGF5cGFsIjp7ImRpc3BsYXlOYW1lIjoiUmFuZG9tQWN0cyIsImNsaWVudElkIjpudWxsLCJwcml2YWN5VXJsIjoiaHR0cDovL2V4YW1wbGUuY29tL3BwIiwidXNlckFncmVlbWVudFVybCI6Imh0dHA6Ly9leGFtcGxlLmNvbS90b3MiLCJiYXNlVXJsIjoiaHR0cHM6Ly9hc3NldHMuYnJhaW50cmVlZ2F0ZXdheS5jb20iLCJhc3NldHNVcmwiOiJodHRwczovL2NoZWNrb3V0LnBheXBhbC5jb20iLCJkaXJlY3RCYXNlVXJsIjpudWxsLCJhbGxvd0h0dHAiOnRydWUsImVudmlyb25tZW50Tm9OZXR3b3JrIjp0cnVlLCJlbnZpcm9ubWVudCI6Im9mZmxpbmUiLCJ1bnZldHRlZE1lcmNoYW50IjpmYWxzZSwiYnJhaW50cmVlQ2xpZW50SWQiOiJtYXN0ZXJjbGllbnQzIiwibWVyY2hhbnRBY2NvdW50SWQiOiJyYW5kb21hY3RzIiwiY3VycmVuY3lJc29Db2RlIjoiVVNEIn0sImNvaW5iYXNlRW5hYmxlZCI6ZmFsc2UsIm1lcmNoYW50SWQiOiJndDltaDI0dno0NDhic3o3IiwidmVubW8iOiJvZmYifQ==",
      "dropin", {
        container: "payment-form"
      });
  });
})(window.angular);










