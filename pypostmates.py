import requests

class PostmateClient(object):
    def __init__(self, customer_id, api_key, server='https://api.postmates.com'):
        self.customer_id = customer_id
        self.api_key = api_key
        self.server = server

    def _post(self, url, data):
        return requests.post('{server}/v1/customers/{customer_id}{url}'.format(
            customer_id=self.customer_id,
            server=self.server,
            url=url
            ),
            data,
            auth=(self.api_key,None)
            )


    def delivery_quotes(self, pickup_address, dropoff_address):
        request = self._post('/delivery_quotes', {
            'pickup_address': pickup_address,
            'dropoff_address': dropoff_address,
            })
        return request

if __name__ == '__main__':

    c = PostmateClient('cus_KNbfJ_GO3X02q-',
            'c57aedcd-73e8-4122-a6a3-5494cd01228c')

    print c.delivery_quotes(
            pickup_address='439 N Wells St, Chicago, IL 60654',
            dropoff_address='North Wells Street, Chicago, IL',
            )

